package at.kolleg.erplite.customermanagement.appservices;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CustomerDto {
    @NotNull
    @Size(min = 10, max = 10)
    private String id;
    @NotNull
    @Size(min = 1)
    private String firstname;
    @Size(min = 1)
    @NotNull
    private String lastname;
    @NotNull
    @Email
    private String email;
    @NotNull
    private List<AddressDto> addressList;
}
