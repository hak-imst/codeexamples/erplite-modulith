package at.kolleg.erplite.sharedkernel.commands;

import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;

public record OrderPaymentCheckCommand(
        @NotNull @Size(min = 10, max = 10, message = "Customer ID must have a length of 10!") String orderID) {

    public OrderPaymentCheckCommand {
        if (orderID == null) throw new IllegalArgumentException("OrderID for checking payment status not valid!");
    }
}

